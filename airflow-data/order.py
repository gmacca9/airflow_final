import sqlite3
import pandas as pd


con = sqlite3.connect(
    '/home/gabriel/airflow_tooltorial/data/Northwind_small.sqlite')
cur = con.cursor()

table = cur.execute(''' SELECT * FROM 'order' ''')

rows = cur.fetchall()

# for row in rows:
#    print(row)

df = pd.read_sql_query(table, con)

con.close()
