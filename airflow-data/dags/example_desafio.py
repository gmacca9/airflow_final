from airflow.utils.edgemodifier import Label
from datetime import datetime, timedelta
from textwrap import dedent
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator
from airflow import DAG
from airflow.models import Variable
import sqlite3
import pandas as pd

# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}


## Do not change the code below this line ---------------------!!#
def export_final_answer():
    import base64

    # Import count
    with open('count.txt') as f:
        count = f.readlines()[0]

    my_email = Variable.get("my_email")
    message = my_email+count
    message_bytes = message.encode('ascii')
    base64_bytes = base64.b64encode(message_bytes)
    base64_message = base64_bytes.decode('ascii')

    with open("final_output.txt", "w") as f:
        f.write(base64_message)
    return None


## Do not change the code above this line-----------------------##


def _order():
    con = sqlite3.connect(
        '/home/gabriel/airflow_tooltorial/data/Northwind_small.sqlite')
    sql = (''' SELECT * FROM 'order' ''')
    df = pd.read_sql_query(sql, con)
    df.to_csv("output_orders.csv")


def _orderdetails():
    con = sqlite3.connect(
        '/home/gabriel/airflow_tooltorial/data/Northwind_small.sqlite')
    cur = con.cursor()

    sql = (''' SELECT * FROM OrderDetail ''')

    order_detail = pd.read_sql_query(sql, con)

    order1 = pd.read_csv(
        '/home/gabriel/airflow_tooltorial/output_orders.csv')

    order_big = pd.merge(order_detail, order1, left_on=[
        'OrderId'], right_on=['Id'], how='left')

    order_big.to_sql('order_big', con, if_exists='replace', index=False)

    sql2 = (
        ''' SELECT SUM(Quantity) FROM order_big WHERE ShipCity = 'Rio de Janeiro' ''')

    df = pd.read_sql_query(sql2, con)
    myfile_csv = df.to_csv("count.txt", header=False, index=False)


with DAG(
    'DesafioAirflow',
    default_args=default_args,
    description='Desafio de Airflow da Indicium',
    schedule_interval=timedelta(days=1),
    start_date=datetime(2021, 1, 1),
    catchup=False,
    tags=['example'],
) as dag:
    dag.doc_md = """
        Esse é o desafio de Airflow da Indicium.
    """

    order = PythonOperator(
        task_id='order',
        python_callable=_order,
        provide_context=True
    )
    order_details = PythonOperator(
        task_id='order_details',
        python_callable=_orderdetails,
        provide_context=True
    )
    my_email = "gabriel.maccarini@indicium.tech"
    export_final_output = PythonOperator(
        task_id='export_final_output',
        python_callable=export_final_answer,
        provide_context=True
    )

order >> order_details >> export_final_output
