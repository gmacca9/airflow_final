Pastas de trabalho referente ao Desafio 3 do programa Lighthouse.

O desafio propõe a criação de uma DAG que lê os dados de uma tabela de um banco de dados, exporta em CSV e realiza uma contagem de pedidos.

Arquivos:
"Requirements.txt": Necessário para os módulos em Python, está disponível na pasta 'airflow-data'.

"example_desafio.py": DAG construída com as tasks propostas no desafio e executada com sucesso no Airflow. Disponível em 'airflow-data/dags'.

"Northwind_small.sqlite": Banco de dados utilizado no desafio para a construção das tasks. Disponível em 'data'. 